package com.example.BlogRestaurante.entities;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table(name = "pais")
public class Pais {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nombrePais")
    @Length(min = 8, max= 50, message = "*El nombre del pais no debe ser mayor a 50 caracteres")
    private String nombre_pais;

    @ManyToOne
    @JoinColumn(name = "id_pais_city")
    private Pais id_pais_city;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre_pais() {
        return nombre_pais;
    }

    public void setNombre_pais(String nombre_pais) {
        this.nombre_pais = nombre_pais;
    }
}
