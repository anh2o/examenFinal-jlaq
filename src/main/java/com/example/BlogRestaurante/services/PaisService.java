package com.example.BlogRestaurante.services;


import com.example.BlogRestaurante.entities.City;
import com.example.BlogRestaurante.entities.Pais;

public interface PaisService {

    Iterable<Pais> listAllPais();
    Pais getPaisById(Integer id);
    Pais savePais(Pais pais);
    void deletePais(Integer id);


}
