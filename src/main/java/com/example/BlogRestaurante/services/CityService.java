package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.Choice;
import com.example.BlogRestaurante.entities.City;

public interface CityService {
    Iterable<City> listAllCities();
    City getCityById(Integer id);
    City saveCity(City city);
    void deleteCity(Integer id);
}
