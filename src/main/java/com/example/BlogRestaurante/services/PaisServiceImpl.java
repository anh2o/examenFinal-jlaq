package com.example.BlogRestaurante.services;

import com.example.BlogRestaurante.entities.City;
import com.example.BlogRestaurante.entities.Pais;
import com.example.BlogRestaurante.repositories.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaisServiceImpl implements PaisService {
    @Autowired
    PaisRepository paisRepository;


    @Override
    public Iterable<Pais> listAllPais() {
        return paisRepository.findAll();
    }


    @Override
    public  Pais savePais(Pais pais) {return  paisRepository.save(pais);}

    @Override
    public Pais getPaisById(Integer id) {return paisRepository.findById(id);}


    @Override
    public void deletePais(Integer id) {
        paisRepository.delete(id);
    }


}
